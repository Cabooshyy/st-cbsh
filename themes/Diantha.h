/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
  /* 8 normal colors */
  "#351e28",  /* black   */
  "#5c7347",  /* red     */
  "#a38089",  /* green   */
  "#C9951B",  /* yellow  */
  "#225073",  /* blue    */
  "#94536d",  /* magenta */
  "#e5e2a3",  /* cyan    */
  "#bfbfc2",  /* white   */

  /* 8 bright colors */
  "#5F5F64",  /* black   */
  "#a6d181",  /* red     */
  "#ed85ae",  /* green   */
  "#C9951B",  /* yellow  */
  "#4cb1ff",  /* blue    */
  "#f5c1ce",  /* magenta */
  "#faf6b2",  /* cyan    */
  "#ffffff",  /* white   */

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#cccccc",
  "#555555",
  "#0A1533",
};
