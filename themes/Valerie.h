/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
  /* 8 normal colors */
  "#3f3049",  /* black   */
  "#953540",  /* red     */
  "#285274",  /* green   */
  "#C9951B",  /* yellow  */
  "#515558",  /* blue    */
  "#94536d",  /* magenta */
  "#ddd01e",  /* cyan    */
  "#bfbfc2",  /* white   */

  /* 8 bright colors */
  "#5F5F64",  /* black   */
  "#ee5466",  /* red     */
  "#4790cd",  /* green   */
  "#C9951B",  /* yellow  */
  "#988e88",  /* blue    */
  "#f5c1ce",  /* magenta */
  "#f0ee79",  /* cyan    */
  "#ffffff",  /* white   */

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#cccccc",
  "#555555",
  "#0A1533",
};
