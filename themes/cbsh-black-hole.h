/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
  /* 8 normal colors */
  "#000000",  /* black   */
  "#87495a",  /* red     */
  "#165099",  /* green   */
  "#C9951B",  /* yellow  */
  "#0D1F73",  /* blue    */
  "#2B8FF0",  /* magenta */
  "#2EA3EE",  /* cyan    */
  "#bfbfc2",  /* white   */

  /* 8 bright colors */
  "#5F5F64",  /* black   */
  "#C2124F",  /* red     */
  "#38A1F0",  /* green   */
  "#C9951B",  /* yellow  */
  "#014AA7",  /* blue    */
  "#2EA3EE",  /* magenta */
  "#7CEEF6",  /* cyan    */
  "#ffffff",  /* white   */

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#cccccc",
  "#555555",
  "#0A1533",
};
