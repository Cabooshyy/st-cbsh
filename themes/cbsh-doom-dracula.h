/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
  /* 8 normal colors */
  "#1E2029",  /* black   */
  "#ff5555",  /* red     */
  "#50fa7b",  /* green   */
  "#f1fa8c",  /* yellow  */
  "#0189cc",  /* blue    */
  "#ff79c6",  /* magenta */
  "#8be9fd",  /* cyan    */
  "#ffffff",  /* white   */

  /* 8 bright colors */
  "#44475a",  /* black   */
  "#ffb86c",  /* red     */
  "#0189cc",  /* green   */
  "#f1fa8c",  /* yellow  */
  "#61bfff",  /* blue    */
  "#bd93f9",  /* magenta */
  "#8be9fd",  /* cyan    */
  "#f8f8f2",  /* white   */

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#e2e2dc",
  "#565761",
  "#1E2029",
};
