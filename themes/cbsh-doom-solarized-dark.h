/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
  /* 8 normal colors */
  "#073642",  /* black   */
  "#dc322f",  /* red     */
  "#859900",  /* green   */
  "#b58900",  /* yellow  */
  "#3F88AD",  /* blue    */
  "#6c71c4",  /* magenta */
  "#204052",  /* cyan    */
  "#626C6C",  /* white   */

  /* 8 bright colors */
  "#56697A",  /* black   */
  "#cb4b16",  /* red     */
  "#35a69c",  /* green   */
  "#b58900",  /* yellow  */
  "#268bd2",  /* blue    */
  "#d33682",  /* magenta */
  "#2aa198",  /* cyan    */
  "#BBBBBB",  /* white   */

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#BBBBBB",
  "#405A61",
  "#002b36",
};
