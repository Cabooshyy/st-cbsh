/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
  /* 8 normal colors */
  "#000000",  /* black   */
  "#8B4627",  /* red     */
  "#9646B8",  /* green   */
  "#C9951B",  /* yellow  */
  "#65538C",  /* blue    */
  "#9C5CAC",  /* magenta */
  "#EB66D5",  /* cyan    */
  "#ffffff",  /* white   */

  /* 8 bright colors */
  "#5F5F64",  /* black   */
  "#87495A",  /* red     */
  "#542269",  /* green   */
  "#C9951B",  /* yellow  */
  "#4B3E69",  /* blue    */
  "#723C87",  /* magenta */
  "#B04C9F",  /* cyan    */
  "#bfbfc2",  /* white   */

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#cccccc",
  "#555555",
  "#241B30",
};
